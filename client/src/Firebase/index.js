import firebase from "firebase/app";
import "firebase/storage";
import "firebase/database";
import "firebase/auth";
import "firebase/messaging";
import "firebase/analytics";
import 'firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyBFF1a3LyAxqRixxxb6KoB6OXvYbfZOFdM",
    authDomain: "universitysearch-961da.firebaseapp.com",
    projectId: "universitysearch-961da",
    storageBucket: "universitysearch-961da.appspot.com",
    messagingSenderId: "654645021728",
    appId: "1:654645021728:web:d1c7aedc8f514cf72a4045",
    measurementId: "G-Z5F14L9BCQ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


const storage = firebase.storage();
const database = firebase.database();
const auth = firebase.auth();
const firestore = firebase.firestore();

// Authentication for Google
var googleProvider = new firebase.auth.GoogleAuthProvider();
// Authentication for Facebook
var facebookProvider = new firebase.auth.FacebookAuthProvider();
// Authentication for Twitter
var twitterProvider = new firebase.auth.TwitterAuthProvider();

export {
    firestore, auth, googleProvider, facebookProvider, twitterProvider, database, storage, firebase as default
}