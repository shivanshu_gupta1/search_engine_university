import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { googleProvider, facebookProvider, auth, storage } from "../../Firebase/index";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import Dropzone from 'react-dropzone';
import Box from '@material-ui/core/Box';
import { v4 as uuid4 } from 'uuid';

// Style Avatar
const avatar = {
    verticalAlign: 'middle',
    width: '150px',
    height: '150px',
    borderRadius: '50%',
    marginBottom: '4px'
}

const useStyles = makeStyles((theme) => ({
    toolbar: {
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbarTitle: {
      flex: 1,
    },
    toolbarSecondary: {
      justifyContent: 'space-between',
      overflowX: 'auto',
    },
    toolbarLink: {
      padding: theme.spacing(1),
      flexShrink: 0,
    },
}));

const Navigation = () => {
    const classes = useStyles();
    const [user, setUser] = React.useState({});
    const [userlogin, setUserLogin] = React.useState();
    const [open, setOpen] = React.useState(false);
    const [openProfile, setOpenProfile] = React.useState(false);
    const [file, setFile] = React.useState([]);
    const [btnMessage, setBtnMessage] = React.useState('Upload Photo');

    const handleClickOpenProfile = () => {
        setOpenProfile(true);
    };
    const handleCloseProfile = () => {
        setOpenProfile(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const signIn = () => {
        auth.signInWithPopup(googleProvider).then((user) => {
        sessionStorage.setItem("userId", user.uid);
        sessionStorage.setItem("userEmail", user.email);
        sessionStorage.setItem("login", true);
        setUserLogin(true);
        handleClose();
        
        }).catch(err => console.log(err));
    }

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
        if (user) {
            setUser(user);
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
        } else {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setUserLogin(false);
        }
        });
    }, []);

    React.useEffect(() => {
        if (file.length > 0) {
            onSubmit();
        } else {
            console.log("N");
        }
    }, [file])

    const signInFacebook = () => {
        auth.signInWithPopup(facebookProvider).then(function(result) {
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            handleClose();
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            console.log(errorMessage);
        });
    }

    const logOut = () => {
        auth.signOut().then(function() {
            setUser({});
            handleCloseProfile();
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
        }).catch(function(error) {
            console.log(error);
        });
    }


    const onSubmit = () => {
        if (file.length > 0) {
            file.forEach(file => {
                var uniquetwoKey = uuid4() + Date.now();
                const uploadTask = storage.ref(`profilepic/${uniquetwoKey}/${file.name}`).put(file);
                uploadTask.on('state_changed', (snapshot) => {
                    const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    setBtnMessage(`Uploading ${progress} %`);
                    
                },
                (error) => {
                    console.log(error);
                },
                async () => {
                    // When the Storage gets Completed
                    const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                    setBtnMessage('Upload Photo');
                    setTimeout(() => setFile([]), 1000);
                    auth.onAuthStateChanged(function(user) {
                        if (user) {
                            user.updateProfile({
                                photoURL: filePath
                            }).then(function() {
                                // Update successful.
                            }).catch(function(error) {
                                // An error happened.
                            });
                        } else {
                            console.log("No");
                        }
                    });
                });
            })
        }
    }

    const handleDrop = async (acceptedFiles) => {
        setFile(acceptedFiles.map(file => file));
    }

    return (
        <>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '2vh' }}>
                    Please Logged In to this website, your email and password are end to end encrypted and secure.
                </DialogContentText>
                
                <Button onClick={signIn} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
                    Continue with Google
                </Button>
                <Button onClick={signInFacebook} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
                    Continue with Facebook
                </Button>
                
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog
                open={openProfile}
                onClose={handleCloseProfile}
                maxWidth={`sm`}
                fullWidth={true}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{`Welcome ${user.displayName}`}</DialogTitle>
                <DialogContent>
                    <Box mr={1} ml={1} mt={3} mb={4}>
                    <center>
                        <img src={user.photoURL} alt="Avatar" style={avatar} />
                        {
                            <Dropzone onDrop={handleDrop}>
                                {({ getRootProps, getInputProps }) => (
                                    <div {...getRootProps({ className: "dropzone" })}>
                                    <input {...getInputProps()} />
                                        <Button color="primary">
                                        {
                                            file.length > 0 ? (
                                                <>
                                                    {btnMessage}
                                                </>
                                            ) : (
                                                <>
                                                    {btnMessage}
                                                </>
                                            )
                                        }
                                        </Button>
                                    </div>
                                )}
                            </Dropzone>
                            }
                        <br />
                        <h5 style={{ color: '#000' }}>{user.displayName}</h5>
                        <h5 style={{ color: '#000' }}>{user.email}</h5>
                        <button onClick={logOut} className="btn btn-block mt-4 btn-lg btn-outline-danger">
                            Logout
                        </button>
                    </center>
                </Box>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseProfile} color="primary" autoFocus>
                    Close
                </Button>
                </DialogActions>
            </Dialog>

        <Toolbar className={classes.toolbar}>
            <a style={{ color: '#000' }} href="/">
                Home
            </a>
            
            <Typography
            component="h2"
            variant="h5"
            color="inherit"
            align="center"
            noWrap
            className={classes.toolbarTitle}
            >
            
            </Typography>
            {
                userlogin ? (
                <>
                    <Avatar className="search-btn" style={{ cursor: 'pointer'}} onClick={handleClickOpenProfile} alt={user.name} src={user.photoURL} />
                </>
                ) : (
                    <Button onClick={handleClickOpen} variant="outlined" size="small">
                    Sign In
                    </Button>
                )
            }
            
        </Toolbar>
        </>
    )
}

export default Navigation
