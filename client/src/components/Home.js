import React from 'react';

// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';

import Button from '@material-ui/core/Button';
import { API_SERVICE, SECRET_KEY } from './config/URI';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import CryptoJS from 'crypto';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const countrys = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];

const avartar = {
    verticalAlign: 'middle',
    width: '50px',
    height: '50px',
    borderRadius: '50%'
}

const UniversityList = ({ universitie, handleClickOpen }) => {
    var country = universitie.country;
    country = country.charAt(0).toUpperCase() + country.slice(1);

    var city = universitie.city;
    city = city.charAt(0).toUpperCase() + city.slice(1);

    var universityType = universitie.type;
    universityType = universityType.charAt(0).toUpperCase() + universityType.slice(1);

    var universityCategory = universitie.category;
    universityCategory = universityCategory.charAt(0).toUpperCase() + universityCategory.slice(1);

    var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
    var eE = mykey.update(universitie._id, 'utf8', 'hex');
    eE += mykey.final('hex');
    var programs = universitie.programs;
    var i = 0;
    return (
            <div class="card card-body mr-5 mt-2 shadow">
                <div>
                    <a target="_blank" href={`/university?id=${eE}`}><img style={avartar}  className="avatar h3" src={universitie.logo} alt="Image" class="img-fluid" /><span style={{ fontSize: '4vh', color: '#000', marginLeft: '4px' }}>{universitie.name} ({`${universityType} ${universityCategory}`})</span>  </a>
                    <br />
                    <br />
                    <h6 style={{ color: '#000' }}>{country} ({city})</h6>
                    <br />
                    <div className="row">
                        <div style={{ color: '#000' }} className="col">
                            { ReactHtmlParser(universitie.about) }
                        </div>
                        <div className="col">
                            {
                                programs.map((p) => (
                                    i = i + 1,
                                    i <= 2 ? (
                                        <>
                                            <li class="list-group-item">
                                                <a href="#!" onClick={() => handleClickOpen(p.eligible)} class="float-right">Check Eligibility</a>
                                                <h5>{p.value}</h5> <br />
                                                <div class="row">
                                                <div class="col">
                                                    <h6>TUITION FEE</h6>
                                                    <h6 style={{ fontFamily: 'auto' }}>$ {p.price}</h6>
                                                </div>
                                                </div>
                                            </li>
                                        </>
                                    ) : null
                                ))
                            }
                        </div>
                    </div>
                    <a target="_blank" href={`/university?id=${eE}`} class="btn btn-primary btn-block btn-lg mt-2 float-right">View</a>
                </div>
            </div>
    )
}



const Home = () => {
    const [universities, setuniversities] = React.useState([]);
    const [loading, setloading] = React.useState(true);
    const [btnclick, setbtnclick] = React.useState(false);
    const [study, setstudy] = React.useState('');
    const [universityname, setuniversityname] = React.useState('');
    const [visa, setvisa] = React.useState('');
    let [eligibility, seteligibility] = React.useState('');
    const [country, setcountry] = React.useState('');
    const [category, setcategory] = React.useState('');
    const [workwhilestudy, setworkwhilestudy] = React.useState('');
    const [campusaccomodation, setcampusaccomodation] = React.useState('');
    const [conditionaloffers, setconditionaloffers] = React.useState('');


    const [open, setOpen] = React.useState(false);
    const handleClickOpen = (eligible) => {
        seteligibility(eligible);
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };


    React.useEffect(() => {
        axios.get(`${API_SERVICE}/api/v1/main/getuniversitites`)
        .then(response => {
            setuniversities(response.data);
            setloading(false);
        })
        .catch(err => console.log(err))
    }, []);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfilterscountry/${country}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [country]);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfiltersvisa/${visa}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [visa]);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfilterscategory/${category}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [category]);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfiltersworkwhilestudy/${workwhilestudy}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [workwhilestudy]);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfilterscampusaccomodation/${campusaccomodation}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [campusaccomodation]);


    React.useEffect(() => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversititesfiltersconditionaloffers/${conditionaloffers}`)
            .then(response => {
                setuniversities(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }, [conditionaloffers]);


    const showUniversitiesList = () => {
        return universities.map(universitie => {
            return <UniversityList handleClickOpen={handleClickOpen} seteligibility={seteligibility} universitie={universitie} key={universitie._id} />
        })
    }


    const search = () => {
        setloading(true);
        setuniversityname('');
        setstudy('');
        setbtnclick(true);

        if (universityname !== '') {
            axios.get(`${API_SERVICE}/api/v1/main/finduniversity/${universityname}`)
                .then(response => {
                    setuniversities(response.data);
                    setloading(false);
                })
                .catch(err => console.log(err))
        }
        
        if (study !== '') {
            axios.get(`${API_SERVICE}/api/v1/main/finduniversitystudy/${study}`)
                .then(response => {
                    setuniversities(response.data);
                    setloading(false);
                })
                .catch(err => console.log(err))
        }

        if ( universityname === '' && study === '' ) {
            axios.get(`${API_SERVICE}/api/v1/main/getuniversitites`)
                .then(response => {
                    setuniversities(response.data);
                    setloading(false);
                })
                .catch(err => console.log(err))
        }
    }



    const alldetails = () => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getuniversitites`)
        .then(response => {
            setuniversities(response.data);
            setloading(false);
        })
        .catch(err => console.log(err))
    }
    
    return (
        <>
            <Navigation />

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Eligibility Criteria for the Programs you are opting for</DialogTitle>
                <DialogContent>
                <DialogContentText className="breakline" id="alert-dialog-description">
                    {eligibility}
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <section className="container mt-4">
                <center>
                    <img className="img img-fluid" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1614580469/search_engine/1j_ojFVDOMkX9Wytexe43D6khvaBpRVJnB7NwXs1M3EMoAJtliEpg...Vi.._ru2riv.png" />
                </center>

                <div className="row mt-4">
                    <div className="col-md">
                        What would you like to Study?
                        <TextField
                            onChange={(event) => setstudy(event.target.value)}
                            margin="normal"
                            value={study}
                            variant="outlined"
                            fullWidth
                            placeholder="What would you like to Study?"
                        />
                    </div>
                    <div className="col-md">
                        Where? e.g. school name or location
                        <Autocomplete
                            freeSolo
                            onInputChange={(event, newInputValue) => {
                                setuniversityname(newInputValue);
                            }}
                            id="free-solo-2-demo"
                            disableClearable
                            fullWidth
                            options={universities.map((option) => option.name)}
                            renderInput={(params) => (
                            <TextField
                                {...params}
                                margin="normal"
                                value={universityname}
                                variant="outlined"
                                placeholder="Where? e.g. school name or location"
                                InputProps={{ ...params.InputProps, type: 'Where? e.g. school name or location' }}
                            />
                            )}
                        />
                    </div>
                </div>                    
                <center>
                    <Button onClick={search} style={{ backgroundColor: '#0074D9', color: '#fff' }} startIcon={<SearchIcon />} size="large" variant="contained" color="primary">
                        Search
                    </Button>
                </center>
            </section>

            <section className="mt-4 w-100 mb-5">
                {
                    btnclick ? (
                        <div className="row">
                            <div className="col-md-4">
                                <div class="card card-body ml-5 sticky shadow mt-2">
                                    <a href="#!" onClick={alldetails} className="card-link text-right h6">
                                        Show All
                                    </a>
                                    <h5 class="card-title text-left">Filters</h5>
                                    <label>Country</label>
                                    <select onChange={(event) => setcountry(event.target.value)} className="form-control" aria-label="Default select example">
                                        <option selected>Select Country</option>
                                        {countrys.map((c) => (
                                            <option value={c}>{c}</option>
                                        ))}
                                    </select>
                                    <label for="exampleFormControlSelect1 labelfilter">Do you have a valid Study Permit / Visa?</label>
                                    <select onChange={(event) => setvisa(event.target.value)} class="form-control" id="exampleFormControlSelect1">
                                        <option value="No">I don't have this</option>
                                        <option value="Yes">Canadian Study Permit or Visitos Visa</option>
                                        <option value="Yes">UK Student Visa (Tire 4) or Short Term Study Visa</option>
                                        <option value="Yes">Australian Study Visa</option>
                                    </select>
                                    <label for="exampleFormControlSelect1 labelfilter">Category?</label>
                                    <select onChange={(event) => setcategory(event.target.value)} class="form-control" id="exampleFormControlSelect1">
                                        <option value="University">University</option>
                                        <option value="Institute">Institute</option>
                                        <option value="College">College</option>
                                    </select>
                                    <label for="exampleFormControlSelect1 labelfilter">Work while study?</label>
                                    <select onChange={(event) => setworkwhilestudy(event.target.value)} class="form-control" id="exampleFormControlSelect1">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                    <label for="exampleFormControlSelect1 labelfilter">Campus Accomodation?</label>
                                    <select onChange={(event) => setcampusaccomodation(event.target.value)} class="form-control" id="exampleFormControlSelect1">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                    <label for="exampleFormControlSelect1 labelfilter">Conditional Offers?</label>
                                    <select onChange={(event) => setconditionaloffers(event.target.value)} class="form-control" id="exampleFormControlSelect1">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                    <br />
                                    {/* <button className="btn btn-primary mt-2 btn-lg">
                                        Apply Filter
                                    </button> */}
                                </div>
                            </div>
                            <div className="col-md">
                                {
                                    loading === true ? (
                                        <center style={{ marginTop: '10%' }}>
                                            <CircularProgress />
                                        </center>
                                    ) : (
                                        <>
                                        {
                                            btnclick === true ? (
                                                <>
                                                
                                                {showUniversitiesList()}
                                                </>
                                            ) : null
                                        }
                                        </>
                                    )
                                }
                            </div>
                        </div>
                    ) : null
                }
            </section>
            <Footer />
        </>
    )
}

export default Home;
