import React from 'react'
// URI
import { API_SERVICE, SECRET_KEY } from './config/URI';
import queryString from 'query-string';
import axios from 'axios';
import CryptoJS from 'crypto';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
const University = ({ location }) => {
    const [universities, setuniversities] = React.useState({});
    const [univcountry, setunivcountry] = React.useState('');
    const [eligibility, seteligibility] = React.useState('');
    const [programs, setprograms] = React.useState([]);

    React.useEffect(() => {
        const { id } = queryString.parse(location.search);
        var mykey = CryptoJS.createDecipher('aes-128-cbc', SECRET_KEY);
        var idE = mykey.update(id, 'hex', 'utf8');
        idE += mykey.final('utf8');
        axios.get(`${API_SERVICE}/api/v1/main/getuniversitydatas/${idE}`)
            .then(response => {
                setuniversities(response.data);
                setprograms(response.data.programs);
                var country = response.data.country;
                country = country.charAt(0).toUpperCase() + country.slice(1);
                setunivcountry(country);
            })
    }, []);


    const [open, setOpen] = React.useState(false);
    const handleClickOpen = (eligible) => {
        setOpen(true);
        seteligibility(eligible);
    };
    const handleClose = () => {
        setOpen(false);
    };


    return (
        <>

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Eligibility Criteria for the Programs you are opting for</DialogTitle>
                <DialogContent>
                <DialogContentText className="breakline" id="alert-dialog-description">
                    { ReactHtmlParser(eligibility) }
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>
            <div className="site-wrap" id="home-section">
                <div className="site-mobile-menu site-navbar-target">
                    <div className="site-mobile-menu-header">
                    <div className="site-mobile-menu-close mt-3">
                        <span className="icon-close2 js-menu-toggle"></span>
                    </div>
                    </div>
                    <div className="site-mobile-menu-body"></div>
                </div>

                <header className="site-navbar light site-navbar-target" role="banner">
                    <div className="container">
                    <div className="row align-items-center position-relative">
                        <div className="col-3">
                        <div className="site-logo">
                            <a href="index.html"><strong>Company Logo</strong></a>
                        </div>
                        </div>
                        <div className="col-9  text-right">
                        <span className="d-inline-block d-lg-none"><a href="#" className=" site-menu-toggle js-menu-toggle py-5 "><span className="icon-menu h3 text-black"></span></a></span>
                        <nav className="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                            <ul className="site-menu main-menu js-clone-nav ml-auto ">
                            </ul>
                        </nav>
                        </div>
                    </div>
                    </div>
                </header>

                <div className="site-section-cover overlay" style={{ backgroundImage: `url(${universities.coverphoto})` }}>
                    <div className="container">
                    <div className="row align-items-center justify-content-center">
                        <div className="col-lg-10 text-center">
                        <h1 className="font-weight-bold">
                            {universities.name}
                        </h1>
                        </div>
                    </div>
                    </div>
                </div>

                <div className="site-section bg-light pb-0">
                    <div className="container">
                    <div className="row align-items-stretch overlap">
                        <div className="col">
                        <div className="h-100">
                            <div className="d-flex align-items-center">
                            <img className="avartaruniversitylogo" src={universities.logo} />
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>


                <div className="site-section bg-light">
                    <div className="ml-4 mr-4">
                    <div className="row">
                        <div className="col-lg-4">
                        <div className="card">
                            <div className="card-body">
                                <h5>{universities.name}</h5>
                                {univcountry}
                                <h6>School ID: {universities._id}</h6>
                                <h6>DLI#: O19359011033</h6>
                                <h6>Founded: 1968</h6>
                                <h6>Type: Public, English Institute</h6>
                                <a target="_blank" href={universities.websiteurl} className="btn btn-primary btn-block mt-2 float-right text-light">Visit Website</a>
                            </div>
                            
                        </div> 
                        </div>

                        <div className="col-lg-8">
                            <div className="card mt-2">
                                <div className="card-body">
                                    <h5>About</h5>
                                    <p>
                                        { ReactHtmlParser(universities.about) }
                                    </p>
                                </div>
                            </div>
                            
                            <div className="card mt-2">
                                <div className="card-body">
                                    <h5>Features</h5>
                                    <ul className="list-group">
                                        <li className="list-group-item">
                                        <span className="float-right">
                                            {
                                                universities.visa === "Yes" ? (
                                                    <img src="https://img.icons8.com/fluent/28/000000/checked.png"/>
                                                ) : (
                                                    <img src="https://img.icons8.com/emoji/28/000000/cross-mark-emoji.png"/>
                                                )
                                            }
                                        </span>

                                        Post Study Visa
                                        
                                        </li>
                                        <li className="list-group-item">
                                        <span className="float-right">
                                            {
                                                universities.conditionaloffer === "Yes" ? (
                                                    <img src="https://img.icons8.com/fluent/28/000000/checked.png"/>
                                                ) : (
                                                    <img src="https://img.icons8.com/emoji/28/000000/cross-mark-emoji.png"/>
                                                )
                                            }
                                        </span>
                                        Conditional Offer
                                        
                                        </li>
                                        <li className="list-group-item">
                                        <span className="float-right">
                                            {
                                                universities.campusaccomodation === "Yes" ? (
                                                    <img src="https://img.icons8.com/fluent/28/000000/checked.png"/>
                                                ) : (
                                                    <img src="https://img.icons8.com/emoji/28/000000/cross-mark-emoji.png"/>
                                                )
                                            }
                                        </span>
                                        
                                        On Campus Accomodation
                                        
                                        </li>
                                        <li className="list-group-item">
                                        <span className="float-right">
                                            {
                                                universities.workwhilestudy === "Yes" ? (
                                                    <img src="https://img.icons8.com/fluent/28/000000/checked.png"/>
                                                ) : (
                                                    <img src="https://img.icons8.com/emoji/28/000000/cross-mark-emoji.png"/>
                                                )
                                            }
                                        </span>
                                        Work While Studying
                                        
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <h4 className="text-center font-weight-bold mt-4 mb-4">
                                Find your perfect program!
                            </h4>

                            {
                                programs.map((p) => (
                                    <>
                                        <div className="card mt-2">
                                            <div className="card-body">
                                                <h5>{p.value}</h5>
                                                <p>
                                                    Application Fee: $ {p.applicationfees} CAD <br />
                                                    Intake: $ {p.intake} CAD <br />
                                                    Total Price: $ {p.price}
                                                </p>
                                                <button onClick={() => handleClickOpen(p.eligible)} className="btn btn-primary float-right">
                                                    Check Eligibility
                                                </button>
                                            </div>
                                        </div>
                                    </>
                                ))
                            }
                        </div>
                    </div>
                    </div>
                </div>


            </div>
        </>
    )
}

export default University
