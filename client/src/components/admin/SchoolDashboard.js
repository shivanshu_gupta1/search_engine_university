import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { mainListItems } from './listItems';
import AddIcon from '@material-ui/icons/Add';
import queryString from 'query-string';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  pos: {
    marginBottom: 12,
    color: '#000000',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '3vh'
  },
  cardroot: {
      minWidth: 230,
  },
}));

export default function SchoolDashboard({ location }) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [allproperties, setAllproperties] = React.useState({});
  const [loading, setLoading] = React.useState(true);
  const [transaction, setTransaction] = React.useState(false);
  const [alertmsg, setalertmsg] = React.useState('');
  const [openProperty, setOpenProperty] = React.useState(true);
  const [country, setcountry] = React.useState('');

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  React.useEffect(() => {
    const { n } = queryString.parse(location.search);
    if (n === 's') {
      setTransaction(true);
      setalertmsg('Property Successfully Registered');
    } else if ( n === 'd' ) {
      setTransaction(true);
      setalertmsg('Property Successfully Removed');
    } else if ( n === 'df' ) {
      setTransaction(true);
      setalertmsg('Floor Successfully Removed');
    }
    axios.get(`${API_SERVICE}/api/v1/main/getallproperty`)
        .then(response => {
            setAllproperties(response.data);
            setLoading(false);
        })
  }, []);

    const [fields, setFields] = React.useState([{ value: null, price: null, duration: null }]);

    function handleChange(i, event) {
        const values = [...fields];
        values[i].value = event.target.value;
        setFields(values);
    }

    function handleChange2(i, event) {
        const values = [...fields];
        values[i].price = event.target.value;
        setFields(values);
    }
    
    function handleChange3(i, event) {
        const values = [...fields];
        values[i].duration = event.target.value;
        setFields(values);
    }
    
    function handleAdd() {
        const values = [...fields];
        values.push({ value: null });
        setFields(values);
    }
    
    function handleRemove(i) {
        const values = [...fields];
        values.splice(i, 1);
        setFields(values);
    }


  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Dashboard
          </Typography>
          
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
        <h2 className="font-weight-bold mt-2 mb-4">
            Add School
        </h2>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Name"
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Address"
              />
            </Grid>
            <Grid item xs={6}>
              <Autocomplete
                  onInputChange={(event, newInputValue) => {
                      setcountry(newInputValue);
                  }}
                  id="country-select-demo"
                  fullWidth
                  options={countries}
                  classes={{
                      option: classes.option,
                  }}
                  autoHighlight
                  getOptionLabel={(option) => option.label}
                  renderOption={(option) => (
                      <React.Fragment>
                          {option.label}
                      </React.Fragment>
                  )}
                  renderInput={(params) => (
                      <TextField
                      {...params}
                      label="Choose a country"
                      variant="outlined"
                      inputProps={{
                          ...params.inputProps,
                          autoComplete: 'new-password', // disable autocomplete and autofill
                      }}
                      />
                  )}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="City"
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Dicipline"
              />
            </Grid>
            
            <Grid item xs={6}>
              <select className="form-control">
                  <option selected>Intakes</option>
                  <option value="Summer">Summer</option>
                  <option value="Fall">Fall</option>
                  <option value="Winter">Winter</option>
              </select>
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Website Url"
              />
            </Grid>
          </Grid>
          <button type="button" className="btn btn-primary mt-5 mb-2 float-right" onClick={() => handleAdd()}>
              Add Courses
          </button>
          {fields.map((field, idx) => {
              return (
              <div key={`${field}-${idx}`}>
                  <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        label="Courses Offered"
                        onChange={e => handleChange(idx, e)}
                        className="mt-2 mb-2"
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <select onChange={e => handleChange3(idx, e)} className="form-control">
                        <option selected>Duration</option>
                        <option value="1 Year">1 Year</option>
                        <option value="2 Year">2 Year</option>
                        <option value="3 Year">3 Year</option>
                        <option value="4 Year">4 Year</option>
                        <option value="5 Year">5 Year</option>
                        <option value="6 Year">6 Year</option>
                    </select>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        label="Application Fees"
                        onChange={e => handleChange2(idx, e)}
                        className="mt-2 mb-2"
                    />
                  </Grid>
                  <button type="button" style={{ backgroundColor: 'red', color: '#fff' }} className="btn btn-delete mt-2 mb-2" onClick={() => handleRemove(idx)}>
                    Delete
                  </button>
                  <hr />
              </div>
              );
          })}

        </Container>
      </main>
    </div>
  );
}

const countries = [
  { label: 'Andorra' },
  { label: 'United Arab Emirates' },
  { label: 'Afghanistan' },
  { label: 'Antigua and Barbuda'  },
  { label: 'Anguilla'  },
  { label: 'Albania' },
  { label: 'Armenia' },
  { label: 'Angola' },
  { label: 'Antarctica' },
  { label: 'Argentina'}, 
  { label: 'American Samoa'  },
  { label: 'Austria' },
  { label: 'Australia'},
  { label: 'Aruba' },
  { label: 'Alland Islands' },
  { label: 'Azerbaijan' },
  { label: 'Bosnia and Herzegovina' },
  { label: 'Barbados'  },
  { label: 'Bangladesh' },
  { label: 'Belgium' },
  { label: 'Burkina Faso' },
  { label: 'Bulgaria' },
  { label: 'Bahrain' },
  { label: 'Burundi' },
  { label: 'Benin' },
  { label: 'Saint Barthelemy' },
  { label: 'Bermuda'  },
  { label: 'Brunei Darussalam' },
  { label: 'Bolivia' },
  { label: 'Brazil'}, 
  { label: 'Bahamas'  },
  { label: 'Bhutan' },
  { label: 'Bouvet Island'}, 
  { label: 'Botswana' },
  { label: 'Belarus' },
  { label: 'Belize' },
  { label: 'Canada' },
  { label: 'Cocos (Keeling) Islands' },
  { label: 'Congo, Democratic Republic of the' },
  { label: 'Central African Republic' },
  { label: 'Congo, Republic of the' },
  { label: 'Switzerland'}, 
  { label: "Cote d'Ivoire" },
  { label: 'Cook Islands' },
  { label: 'Chile' },
  { label: 'Cameroon' },
  { label: 'China' },
  { label: 'Colombia'}, 
  { label: 'Costa Rica' },
  { label: 'Cuba' },
  { label: 'Cape Verde' },
  { label: 'Curacao' },
  { label: 'Christmas Island' },
  { label: 'Cyprus' },
  { label: 'Czech Republic' },
  { label: 'Germany'},
  { label: 'Djibouti' },
  { label: 'Denmark'} ,
  { label: 'Dominica'  },
  { label: 'Dominican Republic'  },
  { label: 'Algeria' },
  { label: 'Ecuador' },
  { label: 'Estonia' },
  { label: 'Egypt' },
  { label: 'Western Sahara' },
  { label: 'Eritrea' },
  { label: 'Spain' },
  { label: 'Ethiopia' },
  { label: 'Finland' },
  { label: 'Fiji' },
  { label: 'Falkland Islands (Malvinas)' },
  { label: 'Micronesia, Federated States of' },
  { label: 'Faroe Islands' },
  { label: 'France' },
  { label: 'Gabon' },
  { label: 'United Kingdom' },
  { label: 'Grenada'  },
  { label: 'Georgia' },
  { label: 'French Guiana' },
  { label: 'Guernsey' },
  { label: 'Ghana' },
  { label: 'Gibraltar' },
  { label: 'Greenland' },
  { label: 'Gambia' },
  { label: 'Guinea' },
  { label: 'Guadeloupe' },
  { label: 'Equatorial Guinea' },
  { label: 'Greece' },
  { label: 'South Georgia and the South Sandwich Islands' },
  { label: 'Guatemala' },
  { label: 'Guam'  },
  { label: 'Guinea-Bissau' },
  { label: 'Guyana' },
  { label: 'Hong Kong' },
  { label: 'Heard Island and McDonald Islands' },
  { label: 'Honduras' },
  { label: 'Croatia' },
  { label: 'Haiti' },
  { label: 'Hungary' },
  { label: 'Indonesia' },
  { label: 'Ireland' },
  { label: 'Israel' },
  { label: 'Isle of Man'}, 
  { label: 'India' },
  { label: 'British Indian Ocean Territory' },
  { label: 'Iraq' },
  { label: 'Iran' },
  { label: 'Iceland' },
  { label: 'Italy' },
  { label: 'Jersey' },
  { label: 'Jamaica'  },
  { label: 'Jordan' },
  { label: 'Japan' },
  { label: 'Kenya' },
  { label: 'Kyrgyzstan' },
  { label: 'Cambodia' },
  { label: 'Kiribati' },
  { label: 'Comoros' },
  { label: 'Saint Kitts and Nevis'  },
  { label: "Korea, Democratic People's Republic of" },
  { label: 'Korea, Republic of' },
  { label: 'Kuwait' },
  { label: 'Cayman Islands'  },
  { label: 'Kazakhstan' } ,
  { label: 'Lebanon' },
  { label: 'Saint Lucia'  },
  { label: 'Liechtenstein' },
  { label: 'Sri Lanka' },
  { label: 'Liberia' },
  { label: 'Lesotho' },
  { label: 'Lithuania' },
  { label: 'Luxembourg' },
  { label: 'Latvia' },
  { label: 'Libya' },
  { label: 'Morocco' },
  { label: 'Monaco' },
  { label: 'Moldova' },
  { label: 'Montenegro' },
  { label: 'Saint Martin' },
  { label: 'Madagascar' },
  { label: 'Marshall Islands' },
  { label: 'Macedonia' },
  { label: 'Mali' },
  { label: 'Myanmar' },
  { label: 'Mongolia' },
  { label: 'Macao' },
  { label: 'Northern Mariana Islands'  },
  { label: 'Martinique' },
  { label: 'Mauritania' },
  { label: 'Montserrat'  },
  { label: 'Malta' },
  { label: 'Mauritius' },
  { label: 'Maldives' },
  { label: 'Malawi' },
  { label: 'Mexico' },
  { label: 'Malaysia' },
  { label: 'Mozambique' },
  { label: 'Namibia' },
  { label: 'New Caledonia' },
  { label: 'Niger' },
  { label: 'Norfolk Island' },
  { label: 'Nigeria' },
  { label: 'Nicaragua' },
  { label: 'Netherlands' },
  { label: 'Norway' },
  { label: 'Nepal' },
  { label: 'Nauru' },
  { label: 'Niue' },
  { label: 'New Zealand' },
  { label: 'Oman' },
  { label: 'Panama' },
  { label: 'Peru' },
  { label: 'French Polynesia' },
  { label: 'Papua New Guinea' },
  { label: 'Philippines' },
  { label: 'Pakistan' },
  { label: 'Poland' },
  { label: 'Saint Pierre and Miquelon' },
  { label: 'Pitcairn' },
  { label: 'Puerto Rico' },
  { label: 'Portugal' },
  { label: 'Palau' },
  { label: 'Paraguay' },
  { label: 'Qatar' },
  { label: 'Reunion' },
  { label: 'Romania' },
  { label: 'Serbia' },
  { label: 'Russian Federation'} ,
  { label: 'Saudi Arabia' },
  { label: 'Solomon Islands' },
  { label: 'Seychelles' },
  { label: 'Sudan' },
  { label: 'Sweden' },
  { label: 'Singapore' },
  { label: 'Saint Helena' },
  { label: 'Slovenia' },
  { label: 'Svalbard and Jan Mayen' },
  { label: 'Slovakia' },
  { label: 'Sierra Leone' },
  { label: 'San Marino' },
  { label: 'Senegal' },
  { label: 'Somalia' },
  { label: 'Suriname' },
  { label: 'South Sudan' },
  { label: 'Sao Tome and Principe' },
  { label: 'El Salvador' },
  { label: 'Sint Maarten (Dutch part)'  },
  { label: 'Syrian Arab Republic' },
  { label: 'Swaziland' },
  { label: 'Turks and Caicos Islands'  },
  { label: 'Chad' },
  { label: 'French Southern Territories' },
  { label: 'Togo' },
  { label: 'Thailand' },
  { label: 'Tajikistan' },
  { label: 'Tokelau' },
  { label: 'Timor-Leste' },
  { label: 'Turkmenistan' },
  { label: 'Tunisia' },
  { label: 'Tonga' },
  { label: 'Turkey' },
  { label: 'Trinidad and Tobago'  },
  { label: 'Tuvalu' },
  { label: 'Taiwan}, Province of China' },
  { label: 'United Republic of Tanzania' },
  { label: 'Ukraine' },
  { label: 'Uganda' },
  { label: 'United States America' },
  { label: 'Uruguay' },
  { label: 'Uzbekistan' },
  { label: 'Holy See (Vatican City State)' },
  { label: 'Saint Vincent and the Grenadines'  },
  { label: 'Venezuela'}, 
  { label: 'British Virgin Islands'  },
  { label: 'US Virgin Islands'  },
  { label: 'Vietnam' },
  { label: 'Vanuatu' },
  { label: 'Wallis and Futuna' },
  { label: 'Samoa' },
  { label: 'Kosovo' },
  { label: 'Yemen' },
  { label: 'Mayotte' },
  { label: 'South Africa' },
  { label: 'Zambia' },
  { label: 'Zimbabwe' },
];