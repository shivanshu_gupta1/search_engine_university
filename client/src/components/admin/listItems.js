import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import SchoolIcon from '@material-ui/icons/School';
export const mainListItems = (
  <div>
    <ListItem onClick={() => window.location.href = "/universitydashboard" } button>
      <ListItemIcon>
        <SchoolIcon />
      </ListItemIcon>
      <ListItemText primary="All University.Schools" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/dashboard" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add University" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/schooldashboard" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add School" />
    </ListItem>
  </div>
);