import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Components
import Home from './components/Home';
import University from './components/University';

import SignInAdmin from './components/admin/SignIn';
import Dashboard from './components/admin/Dashboard';
import SchoolDashboard from './components/admin/SchoolDashboard';
import UniversityAdmin from './components/admin/University';
import UniversityAdminInfo from './components/admin/UniversityInfo';
function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/university" exact component={University} />

      <Route path="/admin" exact component={SignInAdmin} />
      <Route path="/dashboard" exact component={Dashboard} />
      <Route path="/schooldashboard" exact component={SchoolDashboard} />
      <Route path="/universitydashboard" exact component={UniversityAdmin} />
      <Route path="/universityinfo" exact component={UniversityAdminInfo} />
    </Router>
  );
}

export default App;
